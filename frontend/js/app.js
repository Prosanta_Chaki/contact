var app = angular.module("contact", []);

app.controller("ContactCtrl", ["$scope", "$http", "$q", '$timeout',
    function($scope, $http, $q, $timeout){
        $scope.form = {};
        var url = "http://127.0.0.1:8000/api/contact/";
        //get data from api
        var get = function(){
            function onSuccess(response){
                $scope.contacts = response.data;
                console.log(contacts);
            }

            function onError(errors){
                console.log(errors);
            }

            $http.get(url).then(onSuccess).catch(onError);
        };
        // data shorting

        $scope.short = function(value){
            $scope.shortValue = value;
        }

        // Delete
        $scope.deleteModal = function ( id,name ) {
            $scope.modals = {
                
                'id':id,
                'name': name
            }
            
        }

        $scope.delete = function ( id ) {
            function onSuccess(response, status, hearder, config){
                get()

            }

            function onError(error, status, hearder, config){
                console.log(error);
          
            }
            
            $http.delete(url + id + "/").then(onSuccess).catch(onError);
            
        };
        //update from
         $scope.valueInUpdateForm = function(id, name, phone){
           $scope.form2={
                'id':id,
                'name':name,
                'phone':phone
           }
        };
        // Update
         $scope.update = function ( id ) {
            var data = $scope.form2;
            

            function onSuccess(response, status, hearder, config){
                // loading latest contact list
                get();
                // generate success message
                success_message("Contact Updated Successfully");
            }

            function onError(error, status, hearder, config){
                error_message("Something is going wrong");
            }

            $http.put(url + id + "/", data).then(onSuccess).catch(onError)
            $scope.valueInUpdateForm(null,'',null);
            
        };

        // add contact 
        $scope.addContact = function(){
            var data = $scope.form;
            
            function onSuccess(response, status, hearder, config){
                $scope.form ={};
                // re-loading the contact list
                get()
                $scope.success = true;
                $scope.failed = false;

                $timeout(function(){
                    $scope.success = false;
                }, 2000);
            }

            function onError(error, status, hearder, config){
                console.log(error);
                $scope.failed = true;
                $scope.success = false;

                $timeout(function(){
                    $scope.failed = false;
                }, 2000);
            }


            $http.post(url, data).then(onSuccess).catch(onError);

        };


        // loading the list 
        get();
    }
]);

app.filter('capatilize', function(){
    return function (value){
        var i, c, txt = "";
        for (i = 0; i < value.length; i++) {
            c = value[i];
            if (i == 0 || value[i-1]==' ') {
                c = c.toUpperCase();
            }
            txt += c;
        }
        return txt;
    }
})